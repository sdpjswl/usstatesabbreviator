//
//  AppDelegate.h
//  USStatesAbbreviatorExample
//
//  Created by ABS_MAC02 on 13/02/15.
//  Copyright (c) 2015 Sudeep Jaiswal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

