//
//  USStatesAbbreviator.h
//  us states abbreviator
//
//  Created by Sudeep Jaiswal on 24/03/14.
//  Copyright (c) 2014 Sudeep Jaiswal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface USStatesAbbreviator : NSObject

// converts "Las Vegas, Nevada" to "NV"
+ (NSString *)abbreviateStateOfLocationWithName:(NSString *)locationName;

// format: "Nevada - NV"
+ (NSArray *)getStateNamesWithAbbreviations;

@end
