//
//  USStatesAbbreviator.m
//  us states abbreviator
//
//  Created by Sudeep Jaiswal on 24/03/14.
//  Copyright (c) 2014 Sudeep Jaiswal. All rights reserved.
//

#import "USStatesAbbreviator.h"

@implementation USStatesAbbreviator


#pragma mark - Public method

+ (NSString *)abbreviateStateOfLocationWithName:(NSString *)locationName {
    
    NSDictionary *data = [USStatesAbbreviator getData];
    NSArray *array = [locationName componentsSeparatedByString:@", "];
    
    NSString *stateName = [[array lastObject] lowercaseString];
    NSArray *allStateNames = [data allKeys];
    
    for (int i=0; i<[data count]; i++) {
        
        NSRange range = [stateName rangeOfString:[allStateNames objectAtIndex:i]];
        if (range.location != NSNotFound) {
            return [data objectForKey:[allStateNames objectAtIndex:i]];
        }
    }
    return locationName;
}

+ (NSArray *)getStateNamesWithAbbreviations {
    
    NSDictionary *allStates = [USStatesAbbreviator getData];
    __block NSMutableArray *mArr = [[NSMutableArray alloc] init];
    
    [allStates enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        NSString *state = [(NSString *)key capitalizedString];
        NSString *abb = (NSString *)obj;
        NSString *grouped = [NSString stringWithFormat:@"%@ - %@", state, abb];
        [mArr addObject:grouped];
    }];
    
    NSArray *sorted = [mArr sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    return sorted;
}


#pragma mark - Private method

+ (NSDictionary *)getData {
    NSString *directoryPath = [[NSBundle mainBundle] pathForResource:@"states" ofType:@"plist"];
    NSDictionary *states = [[NSDictionary alloc] initWithContentsOfFile:directoryPath];
    return states;
}

@end
