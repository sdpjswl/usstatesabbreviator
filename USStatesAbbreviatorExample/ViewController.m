//
//  ViewController.m
//  USStatesAbbreviatorExample
//
//  Created by ABS_MAC02 on 13/02/15.
//  Copyright (c) 2015 Sudeep Jaiswal. All rights reserved.
//

#import "ViewController.h"
#import "USStatesAbbreviator.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSLog(@"place: %@", [USStatesAbbreviator abbreviateStateOfLocationWithName:@"Los Angeles, California"]);
    
    NSLog(@"states: %@", [USStatesAbbreviator getStateNamesWithAbbreviations]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
