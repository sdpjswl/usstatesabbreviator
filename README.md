US States Abbreviator
=====================

- Returns "NV" for "Las Vegas, Nevada"
- Returns array of all 50 states in format: "California - CA"
